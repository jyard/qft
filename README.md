# CO690 Literature and Research Studies - Quantum Field Theory

A self-directed reading course in quantum field theory (QFT). 

## Instructor
Jon Yard

## Suggested references 
- Zee, Quantum Field Theory in a Nutshell (2nd edition)
- Witten https://arxiv.org/abs/1803.04993 "On some entanglement properties of QFT"

## Grading
- 50% Write up solutions to roughly 10 instructor-approved homework exercises, 
either from Zee or self-generated, such as verifying implicit steps in research 
papers.  The set of submitted problems should differ across students (based on 
background and interest) but some overlap is okay.  Students are allowed 
(even encouraged) to discuss problems and to work together, but each student 
should write up their own solutions (acknowledging all discussions).  
- 40% Final project - A short (e.g. 4-page double column) exposition of literature (topic to be approved by instructor)
- 10% Participation in online discussions

The rough idea behind this grading scheme is for the selected homework problems to support the final project, 
say by filling in necessary background material.  

## Other useful references
- Hollands-Sanders - Entanglement measures and their properties in quantum field theory https://arxiv.org/abs/1702.04924

### Topological theories (possibily with boundary)
- Preskill's topological lecture notes http://theory.caltech.edu/~preskill/ph219/topological
- Kitaev - Fault-tolerant quantum computation with anyons https://arxiv.org/abs/quant-ph/9707021
- Wen - Topological orders and edge excitations in FQH states https://arxiv.org/abs/cond-mat/9506066v2
- Bravyi-Kitaev - Quantum codes on a lattice with boundary https://arxiv.org/abs/quant-ph/9811052
- Kitaev-Kong - Models for gapped boundaries and domain walls https://arxiv.org/abs/1104.5047
- Levin-Wen - String-net condensation: A physical mechanism for topological phases https://arxiv.org/abs/cond-mat/0404617
- Gaiotto-Kulp - Orbifold groupoids https://arxiv.org/abs/2008.05960

### Relativity and spacetimes
- O'Neil - Semi-Riemannian geometry - with applications to relativity 
- Witten - Light rays, singularities and all that https://arxiv.org/abs/1901.03928

### Algebraic QFT
Local integrals of fields generate von Neumann algebras acting on the global Hilbert space.
- https://ncatlab.org/nlab/show/AQFT
- Halvorson-Muger - Algebraic quantum field theory https://arxiv.org/abs/math-ph/0602036
- Fewster-Rejzner - AQFT introduction https://arxiv.org/abs/1904.04051
- Fewster - The split property for quantum field theories in flat and curved spacetimes https://arxiv.org/abs/1601.06936

### Factorization algebras
Mathematically rigorous formulation of perturbative QFT
- Costello-Gwilliam - Factorization algebras in QFT vol 1,2 https://people.math.umass.edu/~gwilliam/ 
- Henriques - Conformal nets are factorization algebras https://arxiv.org/abs/1611.05529

### Relating factorization algebras and algebraic QFT
- Gwilliam-Rejzner - Relating nets and factorization algebras of observables: free field theories https://arxiv.org/abs/1711.06674
- Benini-Perin-Schenkel - Model independent comparison of algebraic QFT and perturbative QFT https://arxiv.org/abs/1903.03396

### Electric-magnetic duality
- 't Hooft - Extended objects in gauge theories 1977
- Montonen-Olive - Magnetic monopoles as gauge particles? 1977
- 't Hooft - On the phase transition towards permanent quark confinement 1978
- 't Hooft - A property of electric and magnetic flux in nonabelian gauge theories 1979 
- Witten - Constraints on supersymmetry breaking

### Extended theories and higher categories
Tip of an iceberg, these references by no means complete.
- Freed and Teleman - Gapped boundaries in three dimensions https://arxiv.org/abs/2006.10200
- Freed - The cobordism hypothesis https://arxiv.org/abs/1210.5100
- Freed and Teleman - Relative quantum field theory https://arxiv.org/abs/1212.1692
- Bartlett, Douglas, Schommer-Pries, Vicary - Modular categories as representations of the 3-dimensional bordism 2-category https://arxiv.org/abs/1509.06811


### Other books and courses 
- Jones - von Neumann Algebras (2015 version)
- Connes - Noncommutative Geometry
- Yeats - A combinatorial perspective on QFT https://www.springer.com/gp/book/9783319475509
- Etingov, Gelaki, Niksych, Ostrik - EGNO - Tensor categories http://www-math.mit.edu/~etingof/egnobookfinal.pdf
- Forward from Talagrand's upcoming book - http://michel.talagrand.net/qft.pdf
- Stanford Math 273 Chatterjee notes on Talagrand - https://statweb.stanford.edu/~souravc/qft-lectures-combined.pdf 
- Folland - QFT: A tourist guide for mathematicians https://bookstore.ams.org/surv-149
- Haag - Local quantum physics
- Peskin & Schroeder - Quantum Field Theory
- Di Francesco, Mathieu & Senechal - Conformal Field Theory (giant yellow book)
- Karshon, Ginzburg & Guillemin - Moment Maps, Cobordisms and Hamiltonian Group Actions
- Psi lectures https://www.perimeterinstitute.ca/training/psi-masters-program/psi-online
- QFT for mathematicians https://www.perimeterinstitute.ca/conferences/qft-mathematicians 
- Riehl - Categorical Homotopy Theory, Elements of $`\infty`$-category theory... http://www.math.jhu.edu/~eriehl/ 
- Homotopy Type Theory https://homotopytypetheory.org/
- 1996-97 Quantum Field Theory program at IAS http://web.math.ucsb.edu/~drm/conferences/QFT/

### Possibly also of interest
- Kallosh and Linde - Strings, black holes and quantum information https://arxiv.org/abs/hep-th/0602061
- Brandao and Kato - Toy model of boundary states with spurious topological entanglement entropy - https://arxiv.org/abs/1911.09819
- The Python's Lunch: geometric obstructions to decoding Hawking radiation https://arxiv.org/abs/1912.00228

